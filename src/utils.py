from itertools import product, combinations_with_replacement
import re


#TODO: Merge two redundant motifs removing functions
def find_minimal(stc):
    """
    Find the set of minimal structures for a given set of RNA secondary structures
    """
    lst = sorted(stc, key=len)
    res = []
    for s in lst:
        try:
            for t in res:
                assert not t in s
            res.append(s)
        except:
#            print(s,t)
            pass
    return res

def remove_redundant(stc):
    lst = sorted(stc, key=lambda t: t.count('*'), reverse=True)
    lst_pattern = map(lambda t: to_pattern(t), lst)
    res = []
    for s in lst_pattern:
        try:
            for t in res:
                assert not t.search(s)
            res.append(re.compile(s.replace('*','.*')))
        except:
 #           print(s.replace('.*','*').replace('a','(').replace('b',')').replace('c','.'),to_stc(t))
            pass
    return list(map(to_stc, res))

def to_pattern(s):
    return s.replace('(','a').replace(')','b').replace('.','c')

def to_stc(inst):
    p = inst.pattern
    return p.replace('.*','*').replace('a','(').replace('b',')').replace('c','.')

def gen_tmp(n_min, n_max):
    assert n_min >= 2, "Minimum allowed value is 2"
    res = {0:[""], 1:["."]}
    for ind in range(2, n_max + 1):
        res[ind] = ["." + s for s in res[ind-1]]
        for j in range(ind-1):
            res[ind] += ['(' + s + ')' + t for s, t in product(res[j], res[ind-2-j])]
    for k in res.keys():
        res[k] = list(map(lambda t: "("+t+")", res[k]))
    return set.union(*[set(res[i]) for i in range(n_min - 2, n_max - 1)])

def gen_motifs(delta, k, n_min, n_max):
    res = set()
    for m in gen_tmp(n_min, n_max):
        for l in combinations_with_replacement(range(1, len(m)), k):
            m_lst = list(m)
            for ind, j in enumerate(l):
                m_lst.insert(ind+j, "(*)")
            res.add(''.join(m_lst))
    # Remove motifs violating minimum distance
    tmp = set()
    for s in res:
        if True in (t in s for t in ['(' + '.' * i + ')' for i in range(delta)]):
            continue
        tmp.add(s)
    return tmp
