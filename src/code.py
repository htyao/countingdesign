#!python3
import pandas as pd
from src.analytics import generate_M, get_dominant_singularity
from src.structure import gen_motifs, find_minimal, remove_redundant

df_ext = pd.read_pickle("db/Counting_0_5_14_grouped.pkl")
df_int = pd.read_pickle("db/Counting_1_2_8_grouped.pkl")

all_ext = gen_motifs(14,0)
all_int = gen_motifs(8,1)

redundant = ['((...)(...))','((...)(....))','((....)(...))','((....)(....))','((...).(...))','(.(...)(...))','((...)(...).)']

# Suboptimal defect <= 0
print("Suboptimal defect <= 0")
m_ext = all_ext - set(df_ext.index[df_ext.d>0])
m_int = all_int - set(df_int.index[df_int.d>0])
motif = remove_redundant(find_minimal(list(set.union(m_ext,m_int))))
print("Local obstruction number:", len(motif))
M = generate_M(["()","(.)","(..)"] + motif)
M -= generate_M(redundant)
s = get_dominant_singularity(M)[0]
print("Dominant Singularity",s)
print("Asymbolic constant", 1/s)

# Probability defect <= 0.5
print("Probability defect <= 0.5")
m_ext = all_ext - set(df_ext.index[df_ext.p>0.5])
m_int = all_int - set(df_int.index[df_int.p>0.5])
motif = remove_redundant(find_minimal(list(set.union(m_ext,m_int))))
print("Local obstruction number:", len(motif))
M = generate_M(["()","(.)","(..)"] + motif)
M -= generate_M(redundant)
s = get_dominant_singularity(M)[0]
print("Dominant Singularity",s)
print("Asymbolic constant", 1/s)

# Probability defect <= 0.1
print("Probability defect <= 0.1")
m_ext = all_ext - set(df_ext.index[df_ext.p>0.9])
m_int = all_int - set(df_int.index[df_int.p>0.9])
motif = remove_redundant(find_minimal(list(set.union(m_ext,m_int))))
print("Local obstruction number:", len(motif))
M = generate_M(["()","(.)","(..)"] + motif)
M -= generate_M(redundant)
s = get_dominant_singularity(M)[0]
print("Dominant Singularity",s)
print("Asymbolic constant", 1/s)

# Probability defect <= 0.01
print("Probability defect <= 0.01")
m_ext = all_ext - set(df_ext.index[df_ext.p>0.99])
m_int = all_int - set(df_int.index[df_int.p>0.99])
motif = remove_redundant(find_minimal(list(set.union(m_ext,m_int))))
print("Local obstruction number:", len(motif))
M = generate_M(["()","(.)","(..)"] + motif)
M -= generate_M(redundant)
s = get_dominant_singularity(M)[0]
print("Dominant Singularity",s)
print("Asymbolic constant", 1/s)
