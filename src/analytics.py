import pandas as pd
from sympy import symbols, collect, expand, fraction, cancel, diff, resultant, solveset, Interval
import sympy
from collections import namedtuple
import argparse
import functools
# import tempfile
from .utils import gen_motifs, find_minimal, remove_redundant

### Defect class
Defect = namedtuple('Defect', ('name', 'label', 'epsilon'))
class SuboptimalDefect(Defect):
    def __new__(cls, e):
       return super().__new__(cls, "Suboptimal", "sub", e)

class ProbabilityDefect(Defect):
    def __new__(cls, e):
       return super().__new__(cls, "Probability", "p", e)

class EnsembleDefect(Defect):
    def __new__(cls, e):
       return super().__new__(cls, "Ensemble", "ens", e)

### Codes for analysis
z, y = symbols("z y")
S = 1/(1-z-y*z**2)

def star(p):
    return 1/(1-p)

def ogf_of_motifs(motifs):
    M = 0
    for m in motifs:
        i = m.count("*")
        j = len(m)-i-2
        M += y**i*z**j
    return collect(M,y)

class Analyse:
    def __init__(self, config, delta = 3):
        """
        Set up database
        """
        self.delta = delta
        settings, db_paths = zip(*(map(self._parse_line, open(config).readlines())))
        self.df = pd.DataFrame()
        self.add_database(*db_paths)
        self.all = functools.reduce(
            lambda x, y: set.union(x, gen_motifs(self.delta, *y)), settings, set())
        self.motifs = set()
        self.obstructions = set()
        self.redundants = set()
        self.defect = None
        self.singularities = []

    def _parse_line(self, line):
        lst = line.strip().split('\t')
        setting = list(map(int, lst[:-1]))
        db_path = lst[-1]
        return setting, db_path

    def add_database(self, *path):
        self.df = self.df.append(pd.concat(map(pd.read_pickle, path)))

    def get_obstructions(self, defect):
        assert self.all, "All should not be vide"
        self.defect = defect
        self.motifs = set(self.df.index[self.df[defect.label] <= defect.epsilon])
        self.obstructions = remove_redundant(find_minimal(self.all - self.motifs))
        #TODO: Implement redundant obstructions counting function
        # with tempfile.NamedTemporaryFile() as tf:
        #    [tf.write((x + '\n').encode()) for x in self.obstructions]
        #    print(tf.name)
        return [x for x in self.obstructions]

    def run_with_redundants(self, redundants):
        self.redundants = redundants
        additional = ["("+"."*i+")" for i in range(self.delta)]
        M = ogf_of_motifs(additional) + ogf_of_motifs(self.obstructions) - ogf_of_motifs(redundants)
        self.singularities = self.get_dominant_singularity(M)
        return 1/self.singularities[0]

    def run_with_correcting(self, correcting):
        additional = ["("+"."*i+")" for i in range(self.delta)]
        M = ogf_of_motifs(additional) + ogf_of_motifs(self.obstructions) - correcting
        self.singularities = self.get_dominant_singularity(M)
        return 1/self.singularities[0]

    def get_dominant_singularity(self, M):
        numerator, _ = fraction(cancel(y-S+M))
        P = collect(expand(numerator),y)
        Q = diff(P,y)
        res = resultant(P,Q)
        solv = solveset(res,z,domain=Interval(1/3,1))
        self.resultant = res
        self.solv = solv
        return list(map(lambda t: t.evalf(), solv))
