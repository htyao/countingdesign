Source code of the project [Counting Design](https://www.lix.polytechnique.fr/~ponty/?page=countingdesigns), in which we aim to estimate the number of RNA secondary
 structures for distinct design objectives.
The manuscript is accepted for [ACM-BCB 2019](http://acm-bcb.org/) and uploaded to [bioRxiv](https://doi.org/10.1101/652313 ).

# Dependencies

The program requires `RNAlib` from the [ViennaRNA package](https://www.tbi.univie.ac.at/RNA/index.html) and Python packages, `pandas` and `sympy`.

# How it works

The program consists of two main files
- `src/build.py` to build the RNA secondary motifs database
- `src/analytics.py` to preceed the analyse using the generating function of secondary structures

## Example

Here, we provide two example scripts `build_ACM_BCB.sh` and `run_analyse_ACM_BCB.py` to reproduce the result presented in the manuscript. 
The first script builds the database of local obstructions of sizes up to $14$ (hairpins) and up to $10$ (internal loops and bulges).
Depending on the system and resources, it may take days (or more than one week) to run the script. User can modify the script by using more
processes to reduce the running time.
The later one shows the basic usage of `src/analytics.py`. It takes the database built as input, then reports the local obstructions number and the 
the analytic constant (inverse of dominant singularity) for different design defects.

## Database building
We provide the script `src/build.py` to build the motif folding result database. See the usage information by calling `src/build.py -h` 

```
usage: build.py [-h] --max MAX [--min MIN] -k K [-p P] [-d DELTA] [-f FILE]

The script aims to build an RNA motif folding result database for given
integers k and n. For each shallow motif of size n+2k with k paired leaves,
the script folds exhaustively each compatible RNA sequence under the
constraint defined on the shallow motif. The script stores the folding result
in XXX.csv and XXX_defect.pickle. If the file name is not given, XXX, by
default, is Counting_{k}_{min}_{max}.

optional arguments:
  -h, --help            show this help message and exit
  --max MAX             maximum value for n (default: None)
  --min MIN             minimum value for n (default: 2)
  -k K                  number of open base pairs (paired leaves) (default:
                        None)
  -p P                  threads number (default: 4)
  -d DELTA, --delta DELTA
                        delta energy value. The script checks if a suboptimal
                        structure exists within delta from MFE (default: 2)
  -f FILE, --file FILE  output file name. Result will be stored in FILE.csv
                        and FILE_grouped.pickle (default: None)
```

NOTE: **Note:** that the size of a motif is $`n+2k`$.

Here are some examples,
- Building internal motifs (motif with one paired leaf) database of size up to $`10`$
    ```bash
    src/build.py --max 8 -k 1
    ```
- Building external motifs database of size up to $`10`$ with $`8`$ threads
    ```bash
    src/build.py --max 10 --min 5 -k 0 -p 8
    ```
    Since the minimal distance between paired positions is $`3`$, the minimum external size is $`5`$.
    
The database is stored in `XXX.csv` and `XXX_grouped.pickle`,
- `XXX.csv` stores the raw data from motif folding. 
    Each raw is the information of constraint folding an RNA sequence. Each column represents, in the order, the MFE motif, MFE, Boltzmann probability, the energy of the second optimal motif, 
RNA sequence, and e\_sec - s\_mfe.

```bash
head XXX.csv 
	s_mfe	mfe	p	e_sec	seq	d
0	((*))	3.7	1.0	inf	AC*GU	inf
1	((*))	5.3	1.0	inf	AU*AU	inf
2	((*))	5.8	1.0	inf	AG*UU	inf
3	((*))	5.5	1.0	inf	AA*UU	inf
4	((*))	3.8	1.0	inf	AG*CU	inf
5	((*))	5.0	1.0	inf	AU*GU	inf
6	((*))	3.5	1.0	inf	UC*GA	inf
7	((*))	5.4	1.0	inf	UG*UA	inf
8	((*))	5.1	1.0	inf	UA*UA	inf
```
**Note:** If there isn't any suboptimal motif within `delta` from the MFE, `e_sec` will be infinity.

- `XXX.pickle` is a `pandas.DataFrame` object, where we store the mxaimum probability, the minimum MFE, and the mximum energy difference a motif can reach among all sequence foldings.
    Below is the code to generate a such dataframe from `XXX.csv`
```python
import pandas as pd
df = pd.read_csv("XXX.csv", sep="\t")
grouped = df.groupby('s_mfe')
new_df = grouped.agg({'p':max, 'mfe':min, 'd': max})
new_df.to_pickle("XXX_grouped.pkl")
```

## Analytic Combinatorics
cf `doc/Motif_ext_int.ipynb`
