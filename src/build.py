#!/usr/bin/python3

from itertools import filterfalse, product, combinations_with_replacement
import argparse
from multiprocessing import Pool
import RNA
import pandas as pd
import numpy as np

md = RNA.md()
md.uniq_ML = 1
kT = (RNA.K0+md.temperature)*RNA.GASCONST*1e-3

BP = [("A","U"),("C","G"),("G","U"),("U","A"),("G","C"),("U","G")]

def boltzmann(e):
    """Return Boltzmann factor of given energy"""
    return np.exp(-e/kT)

def run(inst):
    w, c, delta, ind = inst
    try:
        fc = RNA.fold_compound(w,md)
        fc.hc_add_from_db(c, RNA.CONSTRAINT_DB_ENFORCE_BP | RNA.CONSTRAINT_DB_DEFAULT)
        s_mfe, mfe = fc.mfe()
        _, fee = fc.pf()
        p_mfe = boltzmann(mfe)/boltzmann(fee)
        ens = fc.ensemble_defect(s_mfe)*len(w)
        sub = fc.subopt(delta*100)
	# Case of 
        if len(sub) == 1:
            return ([s_mfe, mfe, p_mfe, np.inf, ens, w],ind)
	# Case of more than one mfe structures
        elif sub[1].energy == mfe:
            return None
        else:
            return ([s_mfe, mfe, p_mfe, sub[1].energy, ens, w],ind)
    except Exception as e:
        print(w,e)

def gen_openpair_seq():
    for a in BP:
        yield a[0]+"AAA"+a[1]

def minimal_completion(w,c,ind_to_insert,seq_to_insert):
    w_lst = list(w)
    c_lst = list(c)
    for ind, j in enumerate(ind_to_insert):
        w_lst.insert(ind+j, seq_to_insert[ind])
        c_lst.insert(ind+j,"(xxx)")
    return ''.join(w_lst), ''.join(c_lst)


def gen_data(max_length, min_length, k, delta):
    for i in range(min_length, max_length+1):
        for a in filterfalse(lambda x: (x[0],x[-1]) not in BP, product(*(["AUCG"]*i))):
            w = "".join(list(a))
            c = "("+"."*(i-2)+")"
            for j in combinations_with_replacement(range(1,i),k):
                for seq in product(gen_openpair_seq(), repeat=k):
                    w1, c1 = minimal_completion(w,c,j,seq)
                    yield (w1,c1,delta,j)

def build_db(max_length, k, path, delta=2, min_length=2, pool_n=4):
    db = open(path, 'w')
    print('\t'.join(["s_mfe","mfe","p","e_sec", "ens", "seq"]), file = db)
    with Pool(pool_n) as pool:
        for res in pool.imap_unordered(run, gen_data(max_length, min_length, k, delta)):
            if res:
                l,ind = res
                l[0], l[-1] = rebuild_str(l[0],l[-1],ind)
                print('\t'.join(map(str, l)), file = db)
    db.close()

def rebuild_str(w,c,ind_lst):
    w_lst = list(w)
    c_lst = list(c)
    for ind, j in enumerate(ind_lst):
        i = 3*ind+j
        w_lst[i+1:i+4] = ['*']
        c_lst[i+1:i+4] = ['*']
    return ''.join(w_lst), ''.join(c_lst)

def extract_db(f):
    df = pd.read_csv(f+".csv", sep="\t")
    df['d'] = df.e_sec - df.mfe
    df.to_csv(f+".csv", sep="\t")
    grouped = df.groupby('s_mfe')
    new = grouped.agg({'p':max, 'mfe':min, 'd': max, "ens": min})
    new['p'] = 1 - new['p']
    new['sub'] = np.exp(-new['d'])
    new[['sub','p','ens','mfe']].to_pickle(f+"_defect.pkl")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description='''
The script aims to build an RNA motif folding result database for 
given integers k and n. For each shallow motif of size n+2k with k 
paired leaves, the script folds exhaustively each compatible RNA 
sequence under the constraint defined on the shallow motif. The 
script stores the folding result in XXX.csv and XXX_defect.pickle.
If the file name is not given, XXX, by default, is Counting_{k}_{min}_{max}.
'''
    )
    parser.add_argument("--max", type = int, required=True,
        help="maximum value for n")
    parser.add_argument("--min", type = int, default = 2, 
        help="minimum value for n")
    parser.add_argument("-k", type = int, required=True,
        help="number of open base pairs (paired leaves)")
    parser.add_argument("-p", type = int, default = 4,
        help="threads number")
    parser.add_argument("-d", "--delta", type = int, default = 2,
        help='''delta energy value. 
	The script checks if a suboptimal structure exists within delta from MFE''')
    parser.add_argument("-f","--file", 
        help="output file name. Result will be stored in FILE.csv and FILE_grouped.pickle")
    args = parser.parse_args()
    if not args.file:
        args.file = "db/Counting_{}_{}_{}".format(args.k,args.min,args.max)
    build_db(args.max, args.k, args.file+".csv", min_length=args.min, pool_n=args.p)
    extract_db(args.file)
    with open("db/defect_db.config", "a+") as config:
        print("{}\t{}\t{}\t{}".format(args.k, args.min, args.max, args.file + "_defect.pkl"),
              file = config)
