#!/usr/bin/python3

from src.analytics import Analyse, ProbabilityDefect, SuboptimalDefect, EnsembleDefect
a = Analyse("db/defect_db.config")
redundants = ['((...)(...))','((...)(....))','((....)(...))','((....)(....))','((...).(...))','(.(...)(...))','((...)(...).)']

# Suboptimal defect
print("Suboptimal defect <= 1")
obstructions = a.get_obstructions(SuboptimalDefect(1))
print("#local obstructions:", len(obstructions))
c = a.run_with_redundants(redundants)
print("Dominant Singularity:", a.singularities[0])
print("Asymbolic constnt:", c)

# Probability defect
print("Probability defect <= 0.5")
obstructions = a.get_obstructions(ProbabilityDefect(0.5))
print("#local obstructions:", len(obstructions))
c = a.run_with_redundants(redundants)
print("Dominant Singularity:", a.singularities[0])
print("Asymbolic constnt:", c)

print("Probability defect <= 0.1")
obstructions = a.get_obstructions(ProbabilityDefect(0.1))
print("#local obstructions:", len(obstructions))
c = a.run_with_redundants(redundants)
print("Dominant Singularity:", a.singularities[0])
print("Asymbolic constnt:", c)

print("Probability defect <= 0.01")
obstructions = a.get_obstructions(ProbabilityDefect(0.01))
print("#local obstructions:", len(obstructions))
c = a.run_with_redundants(redundants)
print("Dominant Singularity:", a.singularities[0])
print("Asymbolic constnt:", c)

